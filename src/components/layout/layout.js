import Nav from '../nav/nav';
import Footer from '../footer/footer';

export default function Layout({ children }) {
  return (
    <>
      <Nav />
      <main>{children}</main>
      <Footer />
    </>
  )
}