const withPlugins = require('next-compose-plugins');
const optimizedImages = require('next-optimized-images');
module.exports = withPlugins([
  [optimizedImages, {
    mozjpeg: {
      quality: 80,
    },
    pngquant: {
      speed: 3,
      strip: true,
      verbose: true,
    },
    imagesPublicPath: process.env.NODE_ENV === 'production' ? '/mjamo/_next/static/images/' : '/_next/static/images/',
  }],
  {
    basePath: process.env.NODE_ENV === 'production' ? '/mjamo' : '',
    assetPrefix: process.env.NODE_ENV === 'production' ? '/mjamo' : '',
    env: {
      ABC: 'abc',
    },
  },
]);