import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(name, amount, unit, price) {
  return { name, amount, unit, price};
}

const rows = [
  createData('Spaghetti', 500, 'g', 1),
  createData('Passierte Tomaten', 500, 'g', 0.89),
  createData('Knoblauch', 3, ' Zehen', 0.35),
  createData('Suppengrün', 1, ' Bündel', 1.39),
];

export default function ShoppingList() {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 300 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Zutaten für's Rezept</TableCell>
            <TableCell align="right">Menge</TableCell>
            <TableCell align="right">Preis</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.amount}{row.unit}</TableCell>
              <TableCell align="right">{row.price}€</TableCell>
            </TableRow>
          ))}
          <TableRow>
            <TableCell align="left">
              <strong>Gesamtpreis</strong>
            </TableCell>
            <Table Cell>
              {null}
            </Table>
            <TableCell align="right">
              3.63€
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}