import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ProTip from '../src/ProTip';
import Link from '../src/Link';
import Copyright from '../src/Copyright';

import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import ListSubheader from '@mui/material/ListSubheader';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';

import Button from '@mui/material/Button';


import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';

export default function Index() {
  return (
    <>
    <Container maxWidth="sm">
      <Box sx={{ my: 4 }}>
        <Typography variant="h4" component="h1" gutterBottom>
          Work In Progress
        </Typography>
        <Button variant="contained" component={Link} noLinkStyle href="/rezept">
          Zu einem Beispiel Rezept
        </Button>
        <br/>
        <br/>
        <Link href="/rezept" color="secondary">
          Zu einem Beispiel Rezept
        </Link>
        {/* <ProTip /> */}
        {/* <Copyright /> */}
      </Box>
      <Box>
      <Autocomplete
      id="highlights-demo"
      sx={{ width: 300, marginLeft: "auto", marginRight: "auto" }}
      options={top100Films}
      getOptionLabel={(option) => option.title}
      renderInput={(params) => (
        <TextField {...params} label="Suche nach einem Rezept" margin="normal" />
      )}
      renderOption={(props, option, { inputValue }) => {
        const matches = match(option.title, inputValue);
        const parts = parse(option.title, matches);

        return (
          <li {...props}>
            <div>
              {parts.map((part, index) => (
                <span
                  key={index}
                  style={{
                    fontWeight: part.highlight ? 700 : 400,
                  }}
                >
                  {part.text}
                </span>
              ))}
            </div>
          </li>
        );
      }}
    />
      </Box>
      </Container>
    <Container maxWidth="xl">
      <Box>
      <ImageList sx={{ width: "90%", maxWidth: "1700px", marginLeft: "auto", marginRight: "auto" }}>
      <ImageListItem key="Subheader" cols={2}>
        <ListSubheader component="div">{null}</ListSubheader>
      </ImageListItem>
      {itemData.map((item) => (
        <ImageListItem key={item.img}>
          <img
            src={`${item.img}?w=248&fit=crop&auto=format`}
            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
          />
          <ImageListItemBar
            title={item.title}
            subtitle={item.author}
            actionIcon={
              <IconButton
                sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                aria-label={`info about ${item.title}`}
              >
                <InfoIcon />
              </IconButton>
            }
          />
        </ImageListItem>
      ))}
    </ImageList>
      </Box>
    </Container>
  </>
  );
}


const itemData = [
  {
    img: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
    title: 'Frühstück',
    author: 'Lecker Lecker Lecker.',
    rows: 2,
    cols: 2,
    featured: true,
  },
  {
    img: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d',
    title: 'Burger',
    author: 'Lecker Lecker Lecker.',
  },
  {
    img: 'https://images.unsplash.com/photo-1522770179533-24471fcdba45',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker. Lecker Lecker Lecker.',
  },
  {
    img: 'https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker.',
    cols: 2,
  },
  {
    img: 'https://images.unsplash.com/photo-1533827432537-70133748f5c8',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker.',
    cols: 2,
  },
  {
    img: 'https://images.unsplash.com/photo-1558642452-9d2a7deb7f62',
    title: 'Honig',
    author: 'Lecker Lecker Lecker.',
    rows: 2,
    cols: 2,
    featured: true,
  },
  {
    img: 'https://images.unsplash.com/photo-1516802273409-68526ee1bdd6',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker.',
  },
  {
    img: 'https://images.unsplash.com/photo-1518756131217-31eb79b20e8f',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker.',
  },
  {
    img: 'https://images.unsplash.com/photo-1597645587822-e99fa5d45d25',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker.',
    rows: 2,
    cols: 2,
  },
  {
    img: 'https://images.unsplash.com/photo-1567306301408-9b74779a11af',
    title: 'Italienisch',
    author: 'Bella Italia.',
  },
  {
    img: 'https://images.unsplash.com/photo-1471357674240-e1a485acb3e1',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker.',
  },
  {
    img: 'https://images.unsplash.com/photo-1589118949245-7d38baf380d6',
    title: 'Beispiel',
    author: 'Lecker Lecker Lecker.',
    cols: 2,
  },
];

const top100Films = [
  {title: 'Spaghetti Bolognese'},
  {title: 'Lasagne'},
  {title: 'Pasta Alfredo'},
  {title: 'Linsen mit Spätzle'},
  {title: 'Tacos'},
  {title: 'Falafel-Baguette'},
  {title: 'Pizzabrötchen'},
  {title: 'Fischburger'},
  {title: 'LIDL-Burger'},
];